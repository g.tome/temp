from django.db import models
import datetime
from django.utils import timezone
# Create your models here.

class Pergunta(models.Model):
    texto = models.CharField(max_length = 200)
    data_pub = models.DateTimeField('Data de publicação')

    def __str__(self):
        return ("{0} - {1}").format(self.id, self.texto)
    def foi_publicada_recentemente(self):
        return self.data_pub >= timezone.now() - datetime.timedelta(days=1)

class alternativa(models.Model):
    texto = models.CharField(max_length = 50)
    quant_votos = models.IntegerField(
        'Quantidade de votos', default = 0
    )
    pergunta = models.ForeignKey(Pergunta, on_delete = models.CASCADE)
    def __str__(self):
        return ("{0} - {1}").format(self.pergunta.texto, self.texto)


