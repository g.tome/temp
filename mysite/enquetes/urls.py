from django.urls import path

from . import views

app_name = 'enquetes'
urlpatterns = [
    # ex.: /
    path('', views.index, name='index'),
    # ex.: detalhes - /enquete/5/
    path('enquete/<int:id_pergunta>/votacao', views.detalhes, name='detalhes'),
     # ex.: votação- /enquete/5/votacao
    path('enquete/<int:id_pergunta>/votacao/', views.votacao, name='votacao'),
     # ex.: resultado - /enquete/5/ resultado
    path('enquete/<int:id_pergunta>/resultado/', views.resultado, name='resultado'),

    ]