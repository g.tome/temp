from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Pergunta, alternativa


# Create your views here.
def index(request):
     lista_enquetes = Pergunta.objects.order_by('-data_pub')[:5]
     contexto = {'lista_enquetes': lista_enquetes}

     return render(request, 'index.html', contexto)


def detalhes(request, id_pergunta):


   # recupera as informações a serem apresentadas
    pergunta = get_object_or_404(Pergunta, pk = id_pergunta)
    # cria o mapa de contexto a ser acessado pelo template
    contexto = { 'pergunta': pergunta }
    # solicita a renderização do template
    return render(request, 'detalhes.html', contexto)



def votacao(request, id_pergunta):
    pergunta = get_object_or_404(Pergunta, pk = id_pergunta)
    try:
        id_opcao = request.POST['alternativa']
        selecionada = pergunta.alternativa_set.get(pk = id_opcao)
    except (KeyError, alternativa.DoesNotExist):
        contexto = {
            'pergunta': pergunta,
            'msg_erro': 'Selecione uma opção válida!'
        }
        return render(request, 'detalhes.html', contexto)
    else:
        selecionada.quant_votos += 1
        selecionada.save()
        return HttpResponseRedirect(
            reverse('enquetes:resultado', args=(pergunta.id,))
        )



    resposta = "<h3>Votação referente à enquete %s</h3>"
    return HttpResponse(resposta % id_pergunta)




def resultado(request, id_pergunta):
    resposta = "<h3>Resultados parciais da enquete %s</h3>"
    return HttpResponse(resposta % id_pergunta)